# Planty

A tiny project to build a watering system for my plants. Main purpose to learn how to connect to sensors, read the data out, create pretty graphs and based on those data set an automated watering schedule for my plants.